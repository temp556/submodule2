
module.exports = fn;

var submodule1 = require('submodule1');

function fn () {
  return submodule1() + ' bar';
}
